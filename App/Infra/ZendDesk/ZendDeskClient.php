<?php

declare(strict_types=1);


class ZendDeskClient
{
    //todo dans une enum plus specialisée
    public CONST ROOM_NAME = '80531287';

    public function __construct(
        //injecter la config, subdomain etc ..
        AuthenticatorInterface $zendDeskAuthenticator
    ) {
        public function send(ResquestInterface $resquest) {

            //todo logger ici ou a part qui catch les exception spécialisées de zendDeskClient, permet de brancher un sentry ou autre
            //$this->getLogger()->addError(var_export($response->user->id, true));
        }
    }
}