<?php

declare(strict_types=1);


use App\Contrat\Infrastructure\ITLHub\Model\Produit;

//possibilité d'hériter un  CreateTicketRequest commun aux 4 types d'utilisateurs
//envie encore plus forte pour le createOrUpdate user qui à encore plus de champs commun
class CreateCustomerTicketRequest implements RequestInterface
{

    private CONST CUSTOMER_KEY = 'customer';

    public function __construct(Customer $customer, $message /** , ... */)
    {

    }

    public function getBody(): string
    {
        return [
            'requester_id' => $response->user->id,
            'subject'      => strlen($message) > 50 ? substr($message, 0, 50) . '...' : $message,
            'comment' =>
                [
                    'body'  => $message
                ],
            'priority'      => 'normal',
            'type'          => 'question',
            'status'        => 'new',
            'custom_fields' => $this->getCustomInfo()
        ];
    }

    private function getCustomInfo() {
        return [
            $customFields[ZendDeskClient::ROOM_NAME] = $roomName;
             $customFields['80924888'] = self::CUSTOMER_KEY;
            $customFields['80531307'] = $reservation->getBookedDate()->format('Y-m-d');


        ]
    }
}