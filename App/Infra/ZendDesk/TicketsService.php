<?php

declare(strict_types=1);

//interface pour injecter ce service dans le domain mais on va plus l'utiliser dans un commandHandler
class TicketsService implements TicketsServiceInterface
{
    /**
     * @var ZendDeskClient
     */
    private $zendDeskClient;

    public function __construct(
        ZendDeskClient $zendDeskClient
    )
    {
        $this->zendDeskClient = $zendDeskClient;
    }

    public function createCustomerTicket(
        Customer $customer,
        $message,
        $reservationNumber,
        $hotel,
        $language,
        $domainConfig)
    {
       //validation des data du customer, ou en amont via des annotation et un middleware

       //todo sur le meme modèle que CreateCustomerTicketRequest
        $this->zendDeskClient->send( new CreateOrUpdateUsertRequest($customer));

        $this->zendDeskClient->send( new CreateCustomerTicketRequest($customer, $message));

        return true;
    }


    public function createHotelTicket(){}

    public function createPressTicket(){}

    public function createPartnersTicket(){}
}