# Background

You just joined Mobility Work and your first job is to refactor an existing class. 
You are alone on this and you may not ask to anyone any help except the given code.
This class is part of a Symfony 2/3/4 project, but that doesn't matter.

> Give a list of prioritised issues that need to be refactored from the most important to the less valuable from an OOP perspective.

Try to apply best PHP and **OOP** practices, and justify your choices.

_Carefully read the existing implementation, and make it solid._

 /**
     * Security => token en CONST => .env
     *
     * Dev:
     * Séparer la responsabilité du client api
     * Séparer la création de la request de l'envoi
     * Factoriser les createOrUpdateUser , voir si nécéssaire la création de ticket
     * sSéparer la responsabilité de l'authenticator de l'api
     *
     *
     * DDD
     * Envoyer des objets domain dans fonctions ( possibilité d'externaliser la validation des objets en amont)
     *  => enum pour faire coller nos classe metier a l'id de l'api
     *
*/