<?php

namespace MobilityWork\Service;

use Zendesk\API\HttpClient as ZendeskAPI;

class ZendeskService extends AbstractService
{
    /**
     * Security => token en CONST => .env
     *
     * Dev:
     * séparer la responsabilité du client api
     * Séparer la création de la request de l'envoi
     * Factoriser les createOrUpdateUser , voir si nécéssaire la création de ticket
     * séparer la responsabilité de l'authenticator de l'api

     *
     * DDD
     * Envoyer des objets domain dans fonctions ( possibilité d'externaliser la validation des objets en amont)
     *  => enum pour faire coller nos classe metier a l'id de l'api
     *
     */

    //todo .env
    const PRODUCTION_SECRET_TOKEN = '7a960781b588403ca32116048238d01c';

    /**
     * @param string $gender
     * @param string $firstName
     * @param string $lastName
     * @param string $phoneNumber
     * @param string $email
     * @param string $message
     * @param string $reservationNumber
     * @param \MobilityWork\Entity\Hotel $hotel
     * @param \MobilityWork\Entity\Language $language
     * @param \MobilityWork\Entity\DomainConfig $domainConfig
     *
     * @return boolean
     */
    public function createCustomerTicket(
        $gender,
        $firstName,
        $lastName,
        $phoneNumber,
        $email,
        $message,
        $reservationNumber,
        $hotel,
        $language,
        $domainConfig)
    {
        $reservation = null;

        if (!empty($reservationNumber)) {
            $reservation = $this->getEntityRepository('Reservation')->getByRef($reservationNumber);

            if ($reservation != null) {
                if ($hotel == null) {
                    $hotel = $reservation->getHotel();
                }
            }
        }

        $customFields = [];
        //todo enum pour faire coller nos classe metier a l'id de l'api
        $customFields['80924888'] = 'customer';
        $customFields['80531327'] = $reservationNumber;


        if ($hotel != null) {
            $hotelContact = $this->getServiceManager()->get('service.hotel_contacts')->getMainHotelContact($hotel);
            //todo dto mapping
            $customFields['80531267'] = $hotelContact != null ? $hotelContact->getEmail() : null;
            $customFields['80918668'] = $hotel->getName();
            $customFields['80918648'] = $hotel->getAddress();
        }

        if ($reservation != null) {
            $roomName = $reservation->getRoom()->getName() . ' ('.$reservation->getRoom()->getType().')';
            //todo dto mapping
            $customFields['80531287'] = $roomName;
            $customFields['80531307'] = $reservation->getBookedDate()->format('Y-m-d');
            $customFields['80924568'] = $reservation->getRoomPrice().' '.$reservation->getHotel()->getCurrency()->getCode();
            $customFields['80918728'] = $reservation->getBookedStartTime()->format('H:i').' - '.$reservation->getBookedEndTime()->format('H:i');
        }

        $customFields['80918708'] = $language->getName();
        //todo separer la responsabilité de l'api
        $client = new ZendeskAPI($this->getServiceManager()->get('Config')['zendesk']['subdomain']);
        //todo separer l'authenticator
        $client->setAuth(
            'basic',
            ['username' => $this->getServiceManager()->get('Config')['zendesk']['username'], 'token' => $this->getServiceManager()->get('Config')['zendesk']['token']]
        );

        $response = $client->users()->createOrUpdate(
            [
                'email' => $email,
                'name' => $firstName.' '.strtoupper($lastName),
                'phone' => !empty($phoneNumber)? $phoneNumber:($reservation != null ? $reservation->getCustomer()->getSimplePhoneNumber() : ''),
                'role' => 'end-user'
            ]
        );

        //$this->getLogger()->addError(var_export($endUser, true));

        $client->tickets()->create(
            [
                'requester_id' => $response->user->id,
                'subject'      => strlen($message) > 50 ? substr($message, 0, 50) . '...' : $message,
                'comment' =>
                    [
                        'body'  => $message
                    ],
                'priority'      => 'normal',
                'type'          => 'question',
                'status'        => 'new',
                'custom_fields' => $customFields
            ]
        );

        return true;
    }

    public function createHotelTicket(
        $gender,
        $firstName,
        $lastName,
        $country,
        $phoneNumber,
        $email,
        $city,
        $website,
        $hotelName,
        $subject,
        $message,
        $language,
        $domainConfig)
    {
        $customFields = [];
        $customFields['80924888'] = 'hotel';
        $customFields['80918668'] = $hotelName;
        $customFields['80918648'] = $city;
        $customFields['80918708'] = $language->getName();

        $client = new ZendeskAPI($this->getServiceManager()->get('Config')['zendesk']['subdomain']);
        $client->setAuth(
            'basic',
            [
                'username' => $this->getServiceManager()->get('Config')['zendesk']['username'],
                'token' => $this->getServiceManager()->get('Config')['zendesk']['token']
            ]
        );

        $response = $client->users()->createOrUpdate(
            [
                'email' => $email,
                'name' => $firstName.' '.strtoupper($lastName),
                'phone' => $phoneNumber,
                'role' => 'end-user',
                'user_fields' => [ 'website' => $website ]
            ]
        );

        $client->tickets()->create(
            [
                'requester_id' => $response->user->id,
                'subject' => strlen($message) > 50 ? substr($message, 0, 50) . '...' : $message,
                'comment' =>
                    [
                        'body' => $message
                    ],
                'priority' => 'normal',
                'type' => 'question',
                'status' => 'new',
                'custom_fields' => $customFields
            ]
        );

        return true;
    }

    public function createPressTicket(
        $gender,
        $firstName,
        $lastName,
        $country,
        $phoneNumber,
        $email,
        $city,
        $media,
        $subject,
        $message,
        $language,
        $domainConfig)
    {
        $customFields = [];
        $customFields['80924888'] = 'press';
        $customFields['80918648'] = $city;
        $customFields['80918708'] = $language->getName();

        $client = new ZendeskAPI($this->getServiceManager()->get('Config')['zendesk']['subdomain']);
        $client->setAuth(
            'basic',
            [
                'username' => $this->getServiceManager()->get('Config')['zendesk']['username'],
                'token' => $this->getServiceManager()->get('Config')['zendesk']['token']
            ]
        );

        $response = $client->users()->createOrUpdate(
            [
                'email' => $email,
                'name' => $firstName.' '.strtoupper($lastName),
                'phone' => $phoneNumber,
                'role' => 'end-user',
                'user_fields' => [ 'press_media' => $media ]
            ]
        );

        try {
            $client->tickets()->create(
                [
                    'requester_id' => $response->user->id,
                    'subject' => strlen($message) > 50 ? substr($message, 0, 50) . '...' : $message,
                    'comment' =>
                        [
                            'body' => $message
                        ],
                    'priority' => 'normal',
                    'type' => 'question',
                    'status' => 'new',
                    'custom_fields' => $customFields
                ]
            );
        } catch (\Exception $e) {
            //todo responsabilité des logs à parts
            $this->getLogger()->addError(var_export($response->user->id, true));
        }

        return true;
    }

    public function createPartnersTicket(
        $gender,
        $firstName,
        $lastName,
        $phoneNumber,
        $email,
        $message,
        $language,
        $domainConfig)
    {
        $customFields = [];
        $customFields['80924888'] = 'partner';
        $customFields['80918708'] = $language->getName();

        $client = new ZendeskAPI($this->getServiceManager()->get('Config')['zendesk']['subdomain']);
        $client->setAuth(
            'basic',
            [
                'username' => $this->getServiceManager()->get('Config')['zendesk']['username'],
                'token' => $this->getServiceManager()->get('Config')['zendesk']['token']
            ]
        );

        $response = $client->users()->createOrUpdate(
            [
                'email' => $email,
                'name' => $firstName.' '.strtoupper($lastName),
                'phone' => $phoneNumber,
                'role' => 'end-user',
            ]
        );

        $client->tickets()->create(
            [
                'requester_id' => $response->user->id,
                'subject' => strlen($message) > 50 ? substr($message, 0, 50) . '...' : $message,
                'comment' =>
                    [
                        'body' => $message
                    ],
                'priority' => 'normal',
                'type' => 'question',
                'status' => 'new',
                'custom_fields' => $customFields
            ]
        );

        return true;
    }
}


